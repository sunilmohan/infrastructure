# SPDX-License-Identifier: AGPL-3.0-or-later

from ..common.builders import BuilderConfigFactory, BuildTarget

_DISTRIBUTION = 'unstable'
_BUILD_STAMP = 'dailydebian'

# This is just for development use with the systemd container
_build_targets = [
    BuildTarget('amd64',
                f'freedombox-{_DISTRIBUTION}_{_BUILD_STAMP}_all-amd64.img.xz')
]

builders = [
    BuilderConfigFactory(
        build_target.name, build_target.artifact_name, build_target.image_size,
        extra_release_components=build_target.extra_release_components).create(
        ) for build_target in _build_targets
]
