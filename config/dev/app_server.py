# SPDX-License-Identifier: AGPL-3.0-or-later

from buildbot.plugins import steps, util

BUILDER_NAME = 'app-server-dev'

_spin_down_commands = [
    ('power off app server',
     ['VBoxManage', 'controlvm', 'app-server', 'poweroff']),
    ('remove attached storage',
     'VBoxManage storageattach app-server --storagectl "SATA Controller" '
     '--port 0 --device 0 --type hdd --medium none'),
    ('close medium', [
        'VBoxManage', 'closemedium',
        '/var/lib/buildbot/workers/gimli/app-server-dev/build/'
        'freedombox-unstable_dailyupstream_all-amd64.vdi'
    ]),
    ('unregister vm', ['VBoxManage', 'unregistervm', 'app-server', '--delete'])
]

_spin_down_steps = [
    steps.ShellCommand(name=name, command=cmd, haltOnFailure=False,
                       flunkOnFailure=False)
    for name, cmd in _spin_down_commands
]

_ARTIFACT_NAME = 'freedombox-unstable_dailyupstream_all-amd64.vdi'

steps = _spin_down_steps + [
    steps.FileDownload(name='download build artifact of virtualbox-amd64-dev',
                       mastersrc='artifacts/' + _ARTIFACT_NAME,
                       workerdest=_ARTIFACT_NAME),
    steps.ShellCommand(
        name="increase disk size to 8 GB", command=[
            'VBoxManage', 'modifymedium',
            'freedombox-unstable_dailyupstream_all-amd64.vdi', '--resize',
            '8000'
        ]),
    steps.FileDownload(name='download the spinup script',
                       mastersrc='scripts/spinup_test_vm.py',
                       workerdest='spinup_test_vm.py'),
    steps.ShellCommand(command=['python3', 'spinup_test_vm.py'],
                       haltOnFailure=True),
]

builders = [
    util.BuilderConfig(name=BUILDER_NAME, workernames=["gimli"],
                       factory=util.BuildFactory(steps))
]
