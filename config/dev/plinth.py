# SPDX-License-Identifier: AGPL-3.0-or-later

from buildbot.plugins import changes, schedulers, steps, util

from .. import conf

_BUILDER_NAME = 'plinth-dev'

_repo_url = conf.conf['repos']['freedombox']['url']
_steps = [
    steps.Git(name="checkout source code", repourl=_repo_url, mode='full',
              method="clobber", shallow=1, haltOnFailure=True),
    steps.ShellCommand(
        name="install build tools",
        command=["sudo", "apt-get", "install", "-y", "git-buildpackage"],
        env={"DEBIAN_FRONTEND": "noninteractive"}, haltOnFailure=True),
    steps.ShellCommand(name="install build dependencies",
                       command=["sudo", "apt-get", "build-dep", "-y", "."],
                       env={"DEBIAN_FRONTEND": "noninteractive"},
                       haltOnFailure=True),
    steps.ShellCommand(
        name="creating cowbuilder environment", command=[
            "sudo", "bash", "-c", "if [ -e /var/cache/pbuilder/base.cow/ ] ; "
            "then cowbuilder update ; else cowbuilder create ; fi"
        ], haltOnFailure=True),
    steps.ShellCommand(name="build debian package",
                       command=["gbp", "buildpackage",
                                "--git-pbuilder"], haltOnFailure=True),
    steps.FileDownload(
        name="download process_freedombox_debian_packages.py",
        mastersrc="scripts/process_freedombox_debian_packages.py",
        workerdest="process_freedombox_debian_packages.py",
        haltOnFailure=True),
    steps.ShellCommand(
        name="rename and move Debian packages",
        command=["python3",
                 "process_freedombox_debian_packages.py"], haltOnFailure=True),
    steps.MultipleFileUpload(
        name="upload build artifacts to main", workersrcs=[
            "freedombox_dev.deb", "freedombox-doc-en_dev.deb",
            "freedombox-doc-es_dev.deb"
        ], masterdest="artifacts/"),
    steps.ShellCommand(name="cleanup", command=["git", "clean", "-f"])
]

# TODO Run tests as sudo
# steps.ShellCommand(
#     name="Set PYTHONPATH", command=
#     'export PYTHONPATH=`{ echo $PWD & python3 -c "import sys; print(\':\'.join(sys.path))"; } | paste -d "" - -`',
#     haltOnFailure=True),

builders = [
    util.BuilderConfig(name=_BUILDER_NAME, workernames=["bilbo"],
                       factory=util.BuildFactory(_steps))
]

# “At minute 0 past hour 4, 8, 12, 16 and 20 on
# Sunday, Monday, Tuesday, Wednesday, Thursday and Friday.”
# 0 4,8,12,16,20 * * 0,1,2,3,4,6
periodic_scheduler = schedulers.Nightly(
    name="once-every-4-hours", branch="main", builderNames=[_BUILDER_NAME],
    onlyIfChanged=False, minute=0, hour=range(4, 24,
                                              4), dayOfWeek=[0, 1, 2, 3, 4, 6])

_force_scheduler = schedulers.ForceScheduler(name="force-plinth-build",
                                             builderNames=[_BUILDER_NAME])

schedulers = [periodic_scheduler, _force_scheduler]

# polling_scheduler = schedulers.SingleBranchScheduler(
#     name="plinth-scheduler",
#     change_filter=util.ChangeFilter(project='plinth', branch='main'),
#     treeStableTimer=None, builderNames=[BUILDER_NAME])

change_sources = [
    changes.GitPoller(_repo_url, workdir='plinth-git-poller', project='plinth',
                      branch='main', pollInterval=300)
]
