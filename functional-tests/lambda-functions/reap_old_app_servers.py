# SPDX-License-Identifier: AGPL-3.0-or-later
'''
If tag:Name startswith "app-server" and running time > 3 hours, then terminate instance
This script should be run periodically.

Assumption: Functional tests would take at most 2.5 hours

Note: This should run in the same timezone as the app servers.
'''

import datetime

import boto3
from dateutil.tz import tzutc

ec2 = boto3.client('ec2')

# LaunchTime returned by the API is in UTC
current_time = datetime.datetime.utcnow().replace(tzinfo=tzutc())


def get_app_servers():
    """Returns a list of InstanceIds of app servers."""

    # The instance should walk, quack and fly like an app server...
    response = ec2.describe_instances(Filters=[{
        'Name': 'instance-state-name',
        'Values': ['running']
    }, {
        'Name': 'instance-type',
        'Values': ['c7g.xlarge']
    }, {
        'Name':
            'tag:Name',
        'Values':
            ['app-server-stable', 'app-server-testing', 'app-server-unstable']
    }])

    lists_of_instances = [r['Instances'] for r in response['Reservations']]
    return [instances[0] for instances in lists_of_instances]


def is_older_than_hours(instance, current_time, hours):
    instance_age = current_time - instance['LaunchTime']
    return instance_age >= datetime.timedelta(hours=hours)


def terminate_instance(instance):
    instance_id = instance['InstanceId']
    ec2.terminate_instances(InstanceIds=[instance_id])
    print(f'Terminated instance {instance_id} successfully.')


def lambda_handler(event, context):
    all_app_servers = get_app_servers()

    for app_server in all_app_servers:
        if is_older_than_hours(app_server, current_time, 3):
            terminate_instance(app_server)
