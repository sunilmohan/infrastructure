#!/usr/bin/env python3
# SPDX-License-Identifier: AGPL-3.0-or-later
"""
Script to update the JSON file needed by Raspberry Pi Imager.

See: https://github.com/raspberrypi/rpi-imager/blob/qml/doc/json-schema/os-list-schema.json
"""

import argparse
import datetime
import json
import pathlib
import subprocess


def main():
    """Parse arguments and update the Imager JSON file."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--image-path', help='Path of the OS image',
                        required=True)
    parser.add_argument('--oslist-path', help='Path of the oslist JSON',
                        required=True)
    parser.add_argument('--name', help='Name of the OS in the oslist',
                        required=True)
    parser.add_argument('--description',
                        help='Description for the OS in the oslist',
                        required=True)
    parser.add_argument('--icon', help='Icon for the OS in the oslist',
                        required=True)
    parser.add_argument('--url',
                        help='URL of the image in the oslist after upload',
                        required=True)
    parser.add_argument('--website', help='URL of the project', required=True)

    arguments = parser.parse_args()
    update_oslist(arguments.image_path, arguments.oslist_path, arguments.name,
                  arguments.description, arguments.icon, arguments.url,
                  arguments.website)


def update_oslist(image_path, oslist_path, name, description, icon, url,
                  website):
    """Update the oslist JSON file."""
    image_path = pathlib.Path(image_path)
    oslist_path = pathlib.Path(oslist_path)

    data = {'os_list': []}
    if oslist_path.exists():
        print('Raspberry Pi Imager oslist JSON file read')
        file_bytes = oslist_path.read_bytes()
        if file_bytes.strip():
            data = json.loads(file_bytes)
    else:
        print('Raspberry Pi Imager oslist JSON file does not exist, creating')

    size, sha256sum = analyze_image(image_path)

    current_os = {
        'name': name,
        'description': description,
        'icon': icon,
        'url': url,
        'website': website,
        'release_date': datetime.datetime.utcnow().strftime('%Y-%m-%d'),
        'extract_size': size,
        'extract_sha256': sha256sum,
        'image_download_size': image_path.stat().st_size,
        'image_download_sha256': _compute_sha256_sum(image_path),
        'devices': ['pi3-64bit', 'pi4-64bit'],
    }

    new_list = []
    added = False
    for os_ in data['os_list']:
        if os_['url'] == url:
            os_ = current_os
            added = True

        new_list.append(os_)

    if not added:
        new_list.append(current_os)

    data['os_list'] = new_list
    oslist_path.write_text(json.dumps(data, indent=4))
    print('Raspberry Pi Imager oslist JSON file written')


def analyze_image(image):
    """Extract the image and return size and sha256sum of the exacted image."""
    print('Exacting image to compute SHA256SUM and size')
    uncompressed_image = image.with_suffix('')
    uncompressed_image.unlink(missing_ok=True)

    subprocess.run(['unxz', '--keep', image], check=True)
    sha256sum = _compute_sha256_sum(uncompressed_image)
    size = uncompressed_image.stat().st_size

    uncompressed_image.unlink()
    return size, sha256sum


def _compute_sha256_sum(file_name):
    """Return the SHA256 sum of a file."""
    output = subprocess.check_output(['sha256sum', file_name])
    return output.decode().partition(' ')[0]


if __name__ == '__main__':
    main()
